# from datetime import datetime
from pony.orm import PrimaryKey, Required, Database  # , Set, Optional


db = Database()
db.bind(provider="sqlite", filename="./database.sqlite", create_db=True)


class Uzivatel(db.Entity):
    email = PrimaryKey(str)
    password = Required(str)
    name = Required(str)


db.generate_mapping(create_tables=True)
